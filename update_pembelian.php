<?php

	include 'koneksi.php';

	$Id_Pembelian = $_GET['Id_Pembelian'];
	$data = mysqli_query($koneksi, "SELECT *FROM pembelian WHERE Id_Pembelian='$Id_Pembelian'");

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Update Data Pembelian</h2>
  <form action="proses_updatepembelian.php" method="POST">
  <?php foreach($data as $value): ?>
    <div class="form-group">
      <label for="email">Id Pembelian:</label>
      <input type="text" class="form-control" id="email" value="<?php echo $value['Id_Pembelian']?>" name="Id_Pembelian" readonly>
    </div>
    <div class="form-group">
      <label for="pwd">Tanggal</label>
      <input type="text" class="form-control" id="pwd" value="<?php echo $value['Tanggal']?>" name="Tanggal">
    </div>
    <div class="form-group">
      <label for="pwd">Id Karyawan</label>
      <input type="text" class="form-control" id="pwd" value="<?php echo $value['Id_Karyawan']?>" name="Id_Karyawan">
    </div>
    <div class="form-group">
      <label for="pwd">Id barang</label>
      <input type="text" class="form-control" id="pwd" value="<?php echo $value['Id_barang']?>" name="Id_barang">
    </div>
    <div class="form-group">
      <label for="pwd">Jumlah Pembelian</label>
      <input type="text" class="form-control" id="pwd" value="<?php echo $value['Jumlah_Pembelian']?>" name="Jumlah_Pembelian">
    </div>
   <!-- <div class="form-group">
      <label for="pwd">Total Pembayaran</label>
      <input type="text" class="form-control" id="pwd" value="<?php //echo $value['Total_Pembayaran']?>" name="Total_Pembayaran">
    </div> -->
    <div class="form-group form-check">
      <label class="form-check-label">
        <input class="form-check-input" type="checkbox" name="remember"> Remember Me
      </label>
    </div>
	<?php endforeach ?>
    <button type="submit" class="btn btn-primary">Update</button>
  </form>
</div>

</body>
</html>