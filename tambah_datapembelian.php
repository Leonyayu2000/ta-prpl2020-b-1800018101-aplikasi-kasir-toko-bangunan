<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Tambah Data Pembelian</h2>
  <form action="proses_inputpembelian.php" method="POST">
    <div class="form-group">
      <label for="email">Id Pembelian:</label>
      <input type="text" class="form-control" id="email" placeholder="Masukkan Id Pembelian" name="Id_Pembelian">
    </div>
    <div class="form-group">
      <label for="pwd">Tanggal</label>
      <input type="date" class="form-control" id="pwd" placeholder="Masukkan Tanggal" name="Tanggal">
    </div>
    <div class="form-group">
      <label for="pwd">Id Karyawan</label>
      <input type="text" class="form-control" id="pwd" placeholder="Id_Karyawan" name="Id_Karyawan">
    </div>
    <div class="form-group">
      <label for="pwd">Id barang</label>
      <input type="text" class="form-control" id="pwd" placeholder="Id_barang" name="Id_barang">
    </div>
    <div class="form-group">
      <label for="pwd">Jumlah Pembelian</label>
      <input type="text" class="form-control" id="pwd" placeholder="Jumlah_Pembelian" name="Jumlah_Pembelian">
    </div>
    <div class="form-group">
      <label for="pwd">Total Peambayaran</label>
      <input type="text" class="form-control" id="pwd" placeholder="Total_Pembayaran" name="Total_Pembayaran">
    </div>
    <div class="form-group form-check">
      <label class="form-check-label">
        <input class="form-check-input" type="checkbox" name="remember"> Remember Me
      </label>
    </div>
    <button type="submit" class="btn btn-primary">Tambah</button>
  </form>
</div>

</body>
</html>
