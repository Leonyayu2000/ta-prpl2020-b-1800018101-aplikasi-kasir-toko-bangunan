<?php

	include 'koneksi.php';

	$Id_Karyawan = $_GET['Id_Karyawan'];
	$data = mysqli_query($koneksi, "SELECT *FROM karyawan WHERE Id_Karyawan='$Id_Karyawan'");

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Update Data Karyawan</h2>
  <form action="proses_updatekaryawan.php" method="POST"> 
  <?php foreach($data as $value): ?>
    <div class="form-group">
      <label for="email">Id Karyawan:</label>
      <input type="text" class="form-control" id="email" value="<?php echo $value['Id_Karyawan']?>" name="Id_Karyawan" readonly>
    </div>
    <div class="form-group">
      <label for="pwd">Nama Karyawan :</label>
      <input type="text" class="form-control" id="pwd" value="<?php echo $value['Nama_Karyawan']?>" name="Nama_Karyawan">
    </div>
    <div class="form-group">
      <label for="pwd">Jenis Kelamin :</label>
      <input type="text" class="form-control" id="pwd" value="<?php echo $value['Jenis_Kelamin']?>" name="Jenis_Kelamin">
    </div>
    <div class="form-group form-check">
      <label class="form-check-label">
        <input class="form-check-input" type="checkbox" name="remember"> Remember Me
      </label>
    </div>
	<?php endforeach ?>
    <button type="submit" class="btn btn-primary">Update</button>
  </form>
</div>

</body>
</html>