<?php

	include 'koneksi.php';

	$Id_barang = $_GET['Id_barang'];
	$data = mysqli_query($koneksi, "SELECT *FROM barang WHERE Id_barang='$Id_barang'");

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Update Data barang</h2>
  <form action="proses_updatebarang.php" method="POST">
  <?php foreach($data as $value): ?>
    <div class="form-group">
      <label for="email">Id barang:</label>
      <input type="text" class="form-control" id="email" value="<?php echo $value['Id_barang']?>" name="Id_barang" readonly>
    </div>
    <div class="form-group">
      <label for="pwd">Nama barang:</label>
      <input type="text" class="form-control" id="pwd" value="<?php echo $value['Nama_barang']?>" name="Nama_barang">
    </div>
    <div class="form-group">
      <label for="pwd">Harga barang:</label>
      <input type="text" class="form-control" id="pwd" value="<?php echo $value['Harga']?>" name="Harga">
    </div>
    <div class="form-group form-check">
      <label class="form-check-label">
        <input class="form-check-input" type="checkbox" name="remember"> Remember Me
      </label>
    </div>
	<?php endforeach ?>
    <button type="submit" class="btn btn-primary">Update</button>
  </form>
</div>

</body>
</html>