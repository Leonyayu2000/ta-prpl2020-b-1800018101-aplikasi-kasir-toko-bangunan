<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Tambah Data barang</h2>
  <form action="proses_inputbarang.php" method="POST">
    <div class="form-group">
      <label for="email">Id barang:</label>
      <input type="text" class="form-control" id="email" placeholder="Masukkan Id barang" name="Id_barang">
    </div>
    <div class="form-group">
      <label for="pwd">Nama barang:</label>
      <input type="text" class="form-control" id="pwd" placeholder="Masukkan Nama barang" name="Nama_barang">
    </div>
    <div class="form-group">
      <label for="pwd">Harga barang:</label>
      <input type="text" class="form-control" id="pwd" placeholder="Masukkan Harga barang" name="Harga">
    </div>
    <div class="form-group form-check">
      <label class="form-check-label">
        <input class="form-check-input" type="checkbox" name="remember"> Remember Me
      </label>
    </div>
    <button type="submit" class="btn btn-primary">Tambah</button>
  </form>
</div>

</body>
</html>
